terraform {
  cloud {
    organization = "terraform_organisation_name"

    workspaces {
      name = "workspace_name"
    }
  }
}