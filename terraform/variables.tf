#######################Variables vsphere

variable "vsphere_user" {
  default = "user_connexion_vcenter"
}

variable "vsphere_password" {
  default = "mot_de_passe_user"
}

variable "vsphere_server" {
  default = "nom_vcenter"
}

variable "vsphere_datacenter" {
  default = "nom_datacenter"
}

variable "vsphere_cluster" {
  default = "nom_cluster"
}

variable "vsphere_template" {
  default = "nom_du_template"
}

variable "tag_primary_category" {
  default = "nom_category_primaire"
}

variable "tag_apps" {
  default = "nom_tag_primaire"
}

variable "tag_role_category" {
  default = "nom_category_role"
}

variable "tag_role_master" {
  default = "nom_tag_master"
}

variable "tag_role_worker" {
  default = "nom_tag_worker"
}

variable "tag_network_category" {
  default = "nom_category_worker"
}

variable "tag_network_lan" {
  default = "nom_tag_net_lan"
}

variable "tag_network_web" {
  default = "nom_tag_net_web"
}


#################################### Variables K8S

####Detail Master 1

variable "master_01_name" {
  default = "nom_master_01"
}

variable "master_01_dts" {
  default = "datastore_vm_master_01"
}

variable "master_01_cpu_number" {
  default = "nombre_vcpu_master_01"
}

variable "master_01_memory_size" {
  default = "taille_memoire_master_01"
}


####Detail Worker 1 (LAN)

variable "worker_lan_01_name" {
  default = "nom_worker_lan_01"
}

variable "worker_lan_01_dts" {
  default = "worker_lan_01_datastore"
}

variable "worker_lan_01_cpu_number" {
  default = "worker_lan_01_vcpu"
}

variable "worker_lan_01_memory_size" {
  default = "worker_lan_01_memory"
}

####Detail Worker 2 (LAN)

variable "worker_lan_02_name" {
  default = "nom_worker_lan_02"
}

variable "worker_lan_02_dts" {
  default = "worker_lan_02_datastore"
}

variable "worker_lan_02_cpu_number" {
  default = "worker_lan_02_vcpu"
}

variable "worker_lan_02_memory_size" {
  default = "worker_lan_02_memory"
}


####Detail Worker 1 (DMZ)

variable "worker_dmz_01_name" {
  default = "nom_worker_web_01"
}

variable "worker_dmz_01_dts" {
  default = "worker_web_01_datastore"
}

variable "worker_dmz_01_cpu_number" {
  default = "worker_web_01_vcpu"
}

variable "worker_dmz_01_memory_size" {
  default = "worker_web_01_memory"
}

####Detail Worker 2 (DMZ)

variable "worker_dmz_02_name" {
  default = "nom_worker_web_02"
}

variable "worker_dmz_02_dts" {
  default = "worker_web_02_datastore"
}

variable "worker_dmz_02_cpu_number" {
  default = "worker_web_02_vcpu"
}

variable "worker_dmz_02_memory_size" {
  default = "worker_web_02_memory"
}

# Variables Network

variable "vlan_lan" {
  default = "nom_port_group_lan"
}

variable "vlan_dmz" {
  default = "nom_port_group_web"
}